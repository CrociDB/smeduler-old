Ext.define('smeduler.controller.NewMedicine', {
    extend: 'Ext.app.Controller',
    
    config: {
        
        refs: {
            medicine: '#addMedicinePanel',
            
            spinnerTimes: '#timesADaySpinner',
            timesFieldset: '#timesFieldset'
        },
        
        control: {
            
            spinnerTimes: {
                change: 'changeSpinnerTimes'
            }
            
        }        
    },
    
    changeSpinnerTimes: function (that, newValue, oldValue, e0pts) {
        var hours = 24 / newValue;
        
        if (this.getTimesFieldset() == null) return;
        
        this.getTimesFieldset().removeAll(true, false);
        
        for (var i = 0; i < newValue; i++)
        {
            this.getTimesFieldset().add({
                xtype: 'timepickerfield',
                label: "Time " + (i + 1),
                increment: 5,
            });
        }        
    }
    
});