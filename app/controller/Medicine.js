Ext.define('smeduler.controller.Medicine', {
    extend: 'Ext.app.Controller',
    
    config: {
        
        refs: {
            medicinePanel: "#medicinePanel",
            addMedicineButton: "#addNewMedicine",
        },
        
        control: {
            
            addMedicineButton: {
                tap: 'addMedicine'
            }
            
        }        
    },
    
    addMedicine: function() {
        this.getMedicinePanel().push({
    		xtype: 'addmedicine'
    	});
        
        navigator.notification.vibrate(1000);
    }
});