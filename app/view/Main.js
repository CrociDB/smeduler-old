Ext.define('smeduler.view.Main', {
    extend: 'Ext.tab.Panel',
    xtype: 'main',
    requires: [
        'Ext.TitleBar'
    ],
    
    config: {
        tabBarPosition: 'bottom',

        items: [
            {
                xtype: "medicinepanel"
            },
            {
                xtype: "historypanel"
            },
            {
                xtype: "settingspanel"
            }
        ]
    }
});
