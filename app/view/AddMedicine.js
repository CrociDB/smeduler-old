Ext.define('smeduler.view.AddMedicine', {
    extend: 'Ext.form.Panel',
    xtype: 'addmedicine',
    
    config: {
        title: 'Add Medicine',
        iconCls: 'time',
        id: 'addMedicinePanel',
        
		scrollable: true,
		styleHtmlContent: true,
        
        items: [
            {
                xtype: 'fieldset',
                title: 'Medicine',
                
                items: [
                    {
                        xtype: 'textfield',
                        name: 'medicineName',
                        label: 'Medicine Name',
                        required: true
                    },
                    {
                        xtype: 'spinnerfield',
                        name: 'timesADay',
                        id: 'timesADaySpinner',
                        label: 'Times a Day',
                        minValue: 1,
                        maxValue: 12,
                        stepValue: 1,
                        cycle: true 
                    },
                    {
                        xtype: 'textareafield',
                        name: 'instructions',
                        label: 'Instructions',
                        required: true
                    },
                ]
            },
            {
                xtype: 'fieldset',
                title: 'Times',
                id: 'timesFieldset',
                
                items: [
                    {
                        xtype: 'timepickerfield',
                        label: "Time",
                        increment: 5,
                    }
                ]
            },
            {
                xtype: 'button',
                ui: 'confirm',
                text: 'Add'
            }
        ]
    }
});