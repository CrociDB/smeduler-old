Ext.define('smeduler.view.Medicine', {
    extend: 'Ext.navigation.View',
    xtype: 'medicinepanel',
    requires: [
        'Ext.TitleBar'
    ],
    
    config: {
        title: 'Medicine',
        iconCls: 'compose',
        id: 'medicinePanel',
        
        items: [
            {
                xtype: 'panel',
                iconCls: 'compose',
                title: 'Medicines',
                
                items: [
                    {
                        xtype: 'toolbar',
                        docked: 'bottom',
                        ui: 'neutral',
                        align: 'center',
                
                        items: [
                            { 
                                xtype: 'spacer' 
                            },
                            {
                                xtype: 'button',
                                id: 'addNewMedicine',
                                iconCls: 'add',
                                text: 'Add Medicine'
                            }
                        ]
                    }
                ]
            },
        ]
    }
});