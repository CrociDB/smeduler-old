Ext.define('smeduler.view.SettingsPanel', {
    extend: 'Ext.Panel',
    xtype: 'settingspanel',
    requires: [
        'Ext.TitleBar'
    ],
    
    config: {
        title: 'Settings',
        iconCls: 'settings',
        
        items: [
            {
                xtype: "titlebar",
                title: "Settings"
            }
        ]
    }
});