Ext.define('smeduler.view.History', {
    extend: 'Ext.Panel',
    xtype: 'historypanel',
    requires: [
        'Ext.TitleBar'
    ],
    
    config: {
        title: 'History',
        iconCls: 'time',
        
        items: [
            {
                xtype: "titlebar",
                title: "Medicine History"
            }
        ]
    }
});